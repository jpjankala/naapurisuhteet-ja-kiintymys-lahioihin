## Monitasoisen regressiomallin rakentaminen gradussa

Käsittelin gradussani monitasoiset analyysit mahdollistavaa survey-aineistoa, johon oli yhdistetty Tilastokeskuksen paikkatietoaineisto. Gradu arvioitiin eximiana.[^1]

Keskeisimmät gradussani käyttämät SPSS-ohjelmat ovat vasemman valikon [Source](https://bitbucket.org/jpjankala/naapurisuhteet-ja-kiintymys-lahioihin/src)-välilehdellä.

- aineiston puhdistaminen ja muuttujamuunnokset
- muuttujien valinta faktorianalyysilla
- puuttuneisuusanalyysi ja moni-imputointi
- faktorianalyysi muuttujien ryhmittelemiseksi
- monitasoinen regressioanalyysi, logistinen ja lineaarinen

Totesin faktoroinnin ja regressioanalyysin avulla 

- *yksilön* kokeman kiintymyksen alueisiin olevan yhteydessä mm. naapurisuhteiden laatuun
- *alueiden* välisten kiintymyserojen olevan vahvasti yhteydessä häiriökokemuksiin ja alueelliseen työttömyyteen
- naapurien harjoittamien kontrollitoimien vähäisen yhteyden kiintymykseen lukuun ottamatta niitä, joilla ei ole naapurisuhteita.

Viimeinen havainto on yllättävä. Projektinani on varmistua havainnon olemassaolosta ja selvittää siihen yhteydessä olevia tekijöitä.

## Graduni erityiset ansiot

Esittelen graduni liitteissä seikkaperäisesti käyttämäni menetelmät monitasoisen regressioanalyysin, faktorianalyysin, kategorisen pääkomponenttianalyysin ja moni-imputoinnin.

Arvioin rakentamieni mallien luotettavuutta luottamusvälien, residuaalidiagnostiikan ja jaetun aineiston avulla. Arvioin tunnuslukujen käytettävyyttä monitasoisessa analyysissa. Arvioin yksittäisten muuttujien käytettävyyttä ja aineiston virhelähteitä.

Käyttämäni käsitteellinen malli on monipuolinen ja perusteltu. Summaan asuinalueisiin kiintymistä koskevaa tutkimusta, Serge Paugamin ja Robert Sampsonin kirjallisuutta sekä suomalaista kaupunkitutkimusta.

Naapurisuhteet ja kiintyminen vanhoihin kerrostalolähiöihin monitasoisella regressioanalyysilla, 2018 Helsingin yliopisto, [http://urn.fi/URN:NBN:fi:hulib-201810023276](http://urn.fi/URN:NBN:fi:hulib-201810023276).

[^1]: "Kaiken kaikkiaan [Eximian] tutkielma ilmentää itsenäistä, kriittistä, innovatiivista tutkimusotetta sekä kykyä jäsentää teoreettisesti laajoja asia- ja ongelmakokonaisuuksia ja taitoa toteuttaa käytännössä hankaliakin tutkimustehtäviä taitavasti asetettujen ongelmien ratkaisemiseksi." (Pro gradu -tutkielman laatiminen ja arvostelu, Valtiotieteellinen tiedekunta 2015)