*Tutkimuskysymyksen 1 logistinen monitasoinen regressioanalyysi, selittäjät lisättiin vaiheittain. Vakio vaihtelee alueittain.
*Tallennettu Pearsonin residuaali, mallin ennakoimat vasteen todennäköisyydet ja arvot.

	GENLINMIXED   /DATA_STRUCTURE SUBJECTS=Lähiö 
	  /FIELDS TARGET=kiin2 TRIALS=NONE ANALYSIS_WEIGHT=wadj1 OFFSET=NONE
	  /TARGET_OPTIONS REFERENCE=0 DISTRIBUTION=BINOMIAL LINK=LOGIT
	  /FIXED  EFFECTS= Zika taltila hallinta sukualue Zruutu_tyotonpertyo
	  suhde
	  tuttuus yhenki
	   USE_INTERCEPT=TRUE   /RANDOM USE_INTERCEPT=TRUE SUBJECTS=Lähiö COVARIANCE_TYPE=VARIANCE_COMPONENTS SOLUTION=FALSE 
	  /BUILD_OPTIONS TARGET_CATEGORY_ORDER=descending INPUTS_CATEGORY_ORDER=descending MAX_ITERATIONS=100 CONFIDENCE_LEVEL=95 DF_METHOD=SATTERTHWAITE COVB=ROBUST PCONVERGE=0.000001(ABSOLUTE) SCORING=0 SINGULAR=0.000000000001
	  /EMMEANS_OPTIONS SCALE=ORIGINAL PADJUST=LSD
	  /save PEARSON_RESIDUALS PREDICTED_PROBABILITY PREDICTED_VALUES. 

*Tutkimuskysymyksen 2 logistinen monitasoinen regressioanalyysi, selittäjät lisättiin vaiheittain. Vakio vaihtelee alueittain.
*Tallennettu Pearsonin residuaali, mallin ennakoimat vasteen todennäköisyydet ja arvot.

	GENLINMIXED   /DATA_STRUCTURE SUBJECTS=Lähiö 
	  /FIELDS TARGET=kiin2 TRIALS=NONE ANALYSIS_WEIGHT=wadj1 OFFSET=NONE
	  /TARGET_OPTIONS REFERENCE=0 DISTRIBUTION=BINOMIAL LINK=LOGIT
	  /FIXED  EFFECTS= Zika taltila hallinta sukualue Zruutu_tyotonpertyo
	  epajar
	  suhde
	  naap sosk
	  sosk*suhde
	   USE_INTERCEPT=TRUE  /RANDOM USE_INTERCEPT=TRUE SUBJECTS=Lähiö COVARIANCE_TYPE=VARIANCE_COMPONENTS SOLUTION=FALSE 
	  /BUILD_OPTIONS TARGET_CATEGORY_ORDER=descending INPUTS_CATEGORY_ORDER=descending MAX_ITERATIONS=100 CONFIDENCE_LEVEL=95 DF_METHOD=SATTERTHWAITE COVB=ROBUST PCONVERGE=0.000001(ABSOLUTE) SCORING=0 SINGULAR=0.000000000001
	  /EMMEANS_OPTIONS SCALE=ORIGINAL PADJUST=LSD
	  /save PREDICTED_PROBABILITY PREDICTED_VALUES PEARSON_RESIDUALS.

*Verrataan residuaaleja oletettuun jakaumaan. Huomataan merkittäviä poikkeamia, jotka johtuvat huonosta päätöksestä diskretoida vaste. Myös outliereiden poistoa olisi voinut harkita.

	weight by wadj1.

	GRAPH
	  /SCATTERPLOT(BIVAR)=PredictedValue WITH PearsonResidual
	  /MISSING=LISTWISE.
	  
		GRAPH
		  /SCATTERPLOT(BIVAR)=PredictedValue_1 WITH PearsonResidual_1
		  /MISSING=LISTWISE.
	  
	PPLOT
	  /VARIABLES=PearsonResidual
	  /NOLOG
	  /NOSTANDARDIZE
	  /TYPE=Q-Q
	  /FRACTION=BLOM
	  /TIES=MEAN
	  /DIST=logistic.
		
		PPLOT
		  /VARIABLES=PearsonResidual_1
		  /NOLOG
		  /NOSTANDARDIZE
		  /TYPE=Q-Q
		  /FRACTION=BLOM
		  /TIES=MEAN
		  /DIST=logistic.

	PPLOT
	  /VARIABLES= PearsonResidual
	  /NOLOG
	  /NOSTANDARDIZE
	  /TYPE=P-P
	  /FRACTION=BLOM
	  /TIES=MEAN
	  /DIST=LOGISTIC.

		PPLOT
		  /VARIABLES= PearsonResidual_1
		  /NOLOG
		  /NOSTANDARDIZE
		  /TYPE=P-P
		  /FRACTION=BLOM
		  /TIES=MEAN
		  /DIST=LOGISTIC.

	MEANS TABLES= PearsonResidual by kiin
	  /CELLS=COUNT MEAN STDDEV RANGE SKEW KURT.

	MEANS TABLES= PearsonResidual_1 by kiin2
	  /CELLS=COUNT MEAN STDDEV RANGE SKEW KURT.

	weight off.

*Lineaarinen monitasoinen regressioanalyysi matalan selitetyn varianssin vuoksi. Vakio vaihtelee alueittain.
*Tallennettu mallin ennakoimat vasteen arvot ja Pearsonin residuaali. Käännetään faktoripisteiden skaala.

	weight by wadj1.
	fre fac1_1 /histogram.
	compute linkiin = fac1_1*(-1).
	exe.
	weight off.

	GENLINMIXED   /DATA_STRUCTURE SUBJECTS=Lähiö 
	  /FIELDS TARGET=linkiin TRIALS=NONE ANALYSIS_WEIGHT=wadj1 OFFSET=NONE
	  /TARGET_OPTIONS REFERENCE=0 DISTRIBUTION=NORMAL LINK=IDENTITY
	  /FIXED  EFFECTS= Zika taltila hallinta sukualue Zruutu_tyotonpertyo
	  epajar
	  suhde
	  naap sosk
	  sosk*suhde
	   USE_INTERCEPT=TRUE   /RANDOM USE_INTERCEPT=TRUE SUBJECTS=Lähiö COVARIANCE_TYPE=VARIANCE_COMPONENTS SOLUTION=FALSE 
	  /BUILD_OPTIONS TARGET_CATEGORY_ORDER=descending INPUTS_CATEGORY_ORDER=descending MAX_ITERATIONS=100 CONFIDENCE_LEVEL=95 DF_METHOD=SATTERTHWAITE COVB=ROBUST PCONVERGE=0.000001(ABSOLUTE) SCORING=0 SINGULAR=0.000000000001
	  /EMMEANS_OPTIONS SCALE=ORIGINAL PADJUST=LSD
	  /save   PREDICTED_VALUES PEARSON_RESIDUALS. 
	  
	GRAPH
	  /SCATTERPLOT(BIVAR)=PredictedValue_2 WITH PearsonResidual_2
	  /MISSING=LISTWISE.
	  
	 PPLOT
	  /VARIABLES= PearsonResidual_2
	  /NOLOG
	  /NOSTANDARDIZE
	  /TYPE=P-P
	  /FRACTION=BLOM
	  /TIES=MEAN
	  /DIST=NORMAL.
	  
	PPLOT
	  /VARIABLES=PearsonResidual_2
	  /NOLOG
	  /NOSTANDARDIZE
	  /TYPE=Q-Q
	  /FRACTION=BLOM
	  /TIES=MEAN
	  
* Täydentävät analyysit logistisella, analysoitu yksi kerrallaan ilman muita täydentäviä.

	weight by wadj1.
	recode ystalue3 (99=0) (0=0) (else=1) into ystaviaalue.
	value labels ystaviaalue 0 'Ei ystäviä alueella tai puuttuu' 1 'Ystäviä alueella'.
	weight off.

	* GENLINMIXED   /DATA_STRUCTURE SUBJECTS=Lähiö 
	  /FIELDS TARGET=kiin2 TRIALS=NONE ANALYSIS_WEIGHT=wadj1 OFFSET=NONE
	  /TARGET_OPTIONS REFERENCE=0 DISTRIBUTION=BINOMIAL LINK=LOGIT
	  /FIXED  EFFECTS= Zika taltila hallinta sukualue Zruutu_tyotonpertyo
	  epajar
	  suhde
	  naap sosk
	  sosk*suhde
	  asaika muutto koulutus tyo ystaviaalue koti                     
	   USE_INTERCEPT=TRUE   /RANDOM USE_INTERCEPT=TRUE SUBJECTS=Lähiö COVARIANCE_TYPE=VARIANCE_COMPONENTS SOLUTION=FALSE 
	  /BUILD_OPTIONS TARGET_CATEGORY_ORDER=descending INPUTS_CATEGORY_ORDER=descending MAX_ITERATIONS=100 CONFIDENCE_LEVEL=95 DF_METHOD=SATTERTHWAITE COVB=ROBUST PCONVERGE=0.000001(ABSOLUTE) SCORING=0 SINGULAR=0.000000000001
	  /EMMEANS_OPTIONS SCALE=ORIGINAL PADJUST=LSD
	  /save PEARSON_RESIDUALS PREDICTED_PROBABILITY PREDICTED_VALUES.