*graduaineiston esikäsittely 3: puuttuneisuuden tarkastaminen, moni-imputointi paikkatietoaineistolla ja vertailuaineistojen teko.
*tehty imputointi liian raskas, älä aja. matala puuttuneisuus huomioiden olisi riittänyt käyttää pelkkiä paikkatietoaineiston muuttujia.

cd 'C:\Gradu'.
GET FILE='prefare_metalla_lokakuu2015_peruspainot_pre1_jp.sav'.
ALTER TYPE ALL(A=AMIN).
DATASET NAME Prefare WINDOW=FRONT.

*Tutkimuskysymys 1:n aineistolle.

* puuttuneisuusanalyysi ennen moni-imputointia, metodina imputoida sisältäen kaikki havainnot, sitten poistaa kaikki paitsi täydelliset vastemuuttujien vastaajat, sitten poistaa ne, joilla jokin summamittari kokonaan tyhjä.
* tämän jälkeen painotettu faktorianalyysi, jolla ensimmäisen tutkimuskysymyksen mukaiset faktoripisteet, joilla monitasoinen regressioanalyysi pisteselittäjillä. 


weight off.

* vasteen puuttuneisuus.
MULTIPLE IMPUTATION  K1501 K1502 K1503 K1504
   /IMPUTE METHOD=NONE
   /MISSINGSUMMARIES  OVERALL VARIABLES (MAXVARS=25 MINPCTMISSING=0) PATTERNS
   /ANALYSISWEIGHT wadj1.

* selittäjät tutkimuskysymys1.
MULTIPLE IMPUTATION  
           K45 /* suhde naapureihin
           K4601 K4602 K4603 K4604 K4605 /* vuorovaikutus
           K4801 K4802 K4803 K4804 K4805 /* yhteishenki
           ystaluecat ysterotuscat k44 /* läheiset
           K49 K5106 K47 /* samanlaisuus ulkonäkö
   /IMPUTE METHOD=NONE
   /MISSINGSUMMARIES  OVERALL VARIABLES (MAXVARS=50 MINPCTMISSING=1) PATTERNS
   /ANALYSISWEIGHT wadj1.

* vaste ja selittäjät tutkimuskysymys1.
MULTIPLE IMPUTATION  
           K1501 K1502 K1503 K1504 /* kiintyminen alueeseen
           K45 /* suhde naapureihin
           K4601 K4602 K4603 K4604 K4605 /* vuorovaikutus
           K4801 K4802 K4803 K4804 K4805 /* yhteishenki
           ystaluecat ysterotuscat sukualue /* läheiset
           K49 K5106 K47 /* samanlaisuus ulkonäkö
   /IMPUTE METHOD=NONE
   /MISSINGSUMMARIES  OVERALL VARIABLES (MAXVARS=50 MINPCTMISSING=1) PATTERNS
   /ANALYSISWEIGHT wadj1.

* puuttuneisuus vaste ja selittäjät tutkimuskysymys2.
weight off.
MULTIPLE IMPUTATION  
          K1501 K1502 K1503 K1504 /* kiintyminen alueeseen
           K45 /* suhde naapureihin
           K4601 K4602 K4603 K4604 K4605 /* vuorovaikutus
           K4801 K4802 K4803 K4804 K4805 /* yhteishenki
           ystaluecat ysterotuscat sukualue /* läheiset
           K49 K5106 K47 /* samanlaisuus ulkonäkö
           K37 K38 /* arvio turvallisuudesta
           K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 /* epäjärjestys
           K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 /* epäjärjestyksen muutos
           K5001 K5002 K5003 K5004 K5005 K5006 /* sosiaalinen kontrolli
   /IMPUTE METHOD=NONE
   /MISSINGSUMMARIES  OVERALL VARIABLES (MAXVARS=50 MINPCTMISSING=1) PATTERNS
   /ANALYSISWEIGHT wadj1.


* filtterimuuttuja vasteelle ja tutkimuskysymykselle 2. valitaan imputointiin kaikki, joissa ei täyttä puuttuneisuutta faktoreilla tai vasteessa. poistuu 3,8%.
weight by wadj1.

COMPUTE miss_vaste = 1.
IF (NVALID( K1501, K1502, K1503, K1504 ) <4) miss_vaste = 0.
EXECUTE.
VARIABLE LABELS miss_vaste 'Puuttuneisuus vasteessa'.
VALUE LABELS miss_vaste 0 'Puuttuvia arvoja vasteen muuttujissa' 1 'Täysi tieto vasteen muuttujista'.

COMPUTE kys2valid = 1.
IF (NVALID( K1501,  K1502,  K1503,  K1504 ) <1) kys1valid = 0. /* 
IF (NVALID( K45, K4601,  K4602,  K4603 , K4604 , K4605 , K47 , K4801,  K4802 , K4803  ) <1) kys2valid = 0. /* sosiaalinen side pl K5106
IF (NVALID( K3501,  K3502,  K3503,  K3504,  K3505,  K3506,  K3507,  K3508, K1304,  K1306 ) <1) kys2valid = 0. /* tyytyväisyys havaittuun järjestykseen
IF (NVALID( K3601,  K3602,  K3603,  K3604,  K3605,  K3606,  K3607,  K3608 ) <1) kys2valid = 0. /* muutos havaitussa järjestyksessä
IF (NVALID( K5001, K5002,  K5003,  K5004,  K5005,  K5006 ) <1) kys2valid = 0. /* sosiaalinen kontrolli pl heikko K5001
EXECUTE.
VARIABLE LABELS kys2valid 'Puuttuneisuus tutkimuskysymys 2'.
VALUE LABELS kys2valid 0 'Täysi puuttuneisuus jollain mittarilla' 1 'Validi tutkimuskysymyksen 2 imputointiin'.

fre kys2valid.

weight by wadj1.


* Katsotaan onko syytä pitää imputoidut kiintymisen arvot, eli von Hippel (2007) mukaisesti: korreloivatko ulkopuoliset, tilastokeskuksen selittäjät vahvasti kiintymyksen kanssa?

*NONPAR CORR
  /VARIABLES=K1501 K1503 K1504 gm_vaki gm_elake gm_tyoton gm_kerrosh gm_lapsital gm_ostov
  /PRINT=BOTH TWOTAIL NOSIG
  /MISSING=PAIRWISE.

*NONPAR CORR
  /VARIABLES=kiin_test gm_vaki gm_elake gm_tyoton gm_kerrosh gm_lapsital gm_ostov
  /PRINT=BOTH TWOTAIL NOSIG
  /MISSING=PAIRWISE.

*korrelaatiot merkitseviä paitsi kerrosten määrä ja keskiostovoima. Spearman rho -kertoimet 0,05–0,22, ei näytä kovin vahvalta.

*ne joilla on huonot suhteet ovat jättäneet joitain pattereita vastaamatta. tarkastele, pitääkö koodata puuttuvat eri tavalla. eli ei pois vaan omaan luokkaansa muuttujien sisällä. luokka -1.
CROSSTABS  kys1valid by K45 /CELLS=count column.


*yhdistetään ennen imputointia K45 liian pienet huonojen ja ristiriitaisten luokat.
recode K45 (1=0) (2=1) (3 thru 4=2) (5=3) (6=9) into K45b.
execute.
VARIABLE LABELS K45b 'Suhteet naapureihin luokiteltu ref.erittäin hyvät'.
VALUE LABELS    K45b 0 'Erittäin hyvät suhteet' 1 'Melko hyvät suhteet' 2 'Huonot suhteet' 3 'Ristiriitaiset suhteet' 9 'Ei suhteita'.


* Sit tutkimuskysymys 2:n aineiston kasaaminen = mi, aineiston tallentaminen, faktoripisteet, regressio.

DATASET ACTIVATE prefare.

weight off.
filter off.











* Moni-imputoidaan muuttujat tutkimuskysymykselle 2.

USE ALL.
FILTER BY kys2valid.
EXECUTE.

*asetetaan satunnainen seed, jotta saadaan samat arvot mikäli lasku tulisi ajaa uudelleen samoilla muuttujilla. osa muuttujista pelkkinä selittäjinä, paino mukana.

weight off.

SET RNG=MT MTINDEX=18012018.
MULTIPLE IMPUTATION            K1501 K1502 K1503 K1504 /* kiintyminen alueeseen
           K45b /* suhde naapureihin
           K4601 K4602 K4603 K4604 K4605 /* vuorovaikutus
           K4801 K4802 K4803 K4804 K4805 /* yhteishenki
           K49 K5106 K47 /* samanlaisuus ulkonäkö
           K37 K38 /* arvio turvallisuudesta
           K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 /* epäjärjestys
           K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 /* epäjärjestyksen muutos
           K5001 K5002 K5003 K5004 K5005 K5006 /* sosiaalinen kontrolli
           K25 K26 K27 /* taloudellinen tila
ruutu_vaki ruutu_tyotonpertyo ruutu_keskiostov ruutu_perusperkaikki ruutu_korkperkaikki ruutu_asval 
ika sp koulutus hallinta talo koti asaika tyo ystaluecat ysterotuscat sukualue
  /ANALYSISWEIGHT wadj1
  /IMPUTE METHOD=FCS MAXITER= 10 NIMPUTATIONS=5 SCALEMODEL=LINEAR INTERACTIONS=none SINGULAR=1E-012 
    MAXPCTMISSING=10  MAXMODELPARAM = 1500
  /CONSTRAINTS  K1501 K1502 K1503 K1504 /* kiintyminen alueeseen
           K45b /* suhde naapureihin
           K4601 K4602 K4603 K4604 K4605 /* vuorovaikutus
           K4801 K4802 K4803 K4804 K4805 /* yhteishenki
           K49 K5106 K47 /* samanlaisuus ulkonäkö
           K37 K38 /* arvio turvallisuudesta
           K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 /* epäjärjestys
           K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 /* epäjärjestyksen muutos
           K5001 K5002 K5003 K5004 K5005 K5006 /* sosiaalinen kontrolli
           K25 K26 K27 /* taloudellinen tila
            ( ROLE=DEP)
  /CONSTRAINTS ruutu_vaki ruutu_tyotonpertyo ruutu_keskiostov ruutu_perusperkaikki ruutu_korkperkaikki ruutu_asval 
ika sp koulutus hallinta talo koti asaika tyo ystaluecat ysterotuscat sukualue ( ROLE=IND)
  /MISSINGSUMMARIES OVERALL VARIABLES 
  /IMPUTATIONSUMMARIES MODELS DESCRIPTIVES 
  /OUTFILE IMPUTATIONS='prefare_jp0imp.sav'.

FILTER OFF.
USE ALL.

GET FILE='prefare_jp0imp.sav'.
DATASET NAME prefare_jp0imp WINDOW=FRONT.

DATASET ACTIVATE prefare_jp0imp.

*yhdistetään moni-imputoinnista saadut iteraatiot: otetaan id:n mukainen keskiarvo. kopioidaan alkuperäisistä muuttujista tekstit, jätetään imputoitu aineisto varalle ja poistetaan väliaikaiset muuttujat ja nyt turhat iteraatiot.

* weight by wadj1. /* painottamattomana tai mediaani antaa hassuja arvoja.

SORT CASES by id.
AGGREGATE
  /OUTFILE=* MODE=ADDVARIABLES
  /PRESORTED
  /BREAK=id
  /suhde =MEDIAN( K45b ) /* aggregoidaan pääselittäjä mediaaniinsa, koska "ei suhteita" asettuu hyvät/huonot suhteet -akselin ulkopuolelle
  /ylpea =MEAN( K1501 )
  /hapea=MEAN( K1502 )
  /kaipaus =MEAN( K1503 )
  /pidan =MEAN( K1504 )
  /juttelu =MEAN( K4601 )
  /kyla =MEAN( K4602 )
  /napu =MEAN( K4603 )
  /ytyo =MEAN( K4604 )
  /jarj =MEAN( K4605 )
  /ulkon =MEAN( K47 )
  /naut =MEAN( K4801 )
  /nhenki =MEAN( K4802 )
  /luot =MEAN( K4803 ) 
  /toimeen =MEAN( K4804 )
  /arvo =MEAN( K4805 ) 
  /sama =MEAN( K49 ) 
  /hengentarv =MEAN(K5106)
  /roska =MEAN( K3501 )
  /juop = MEAN( K3502 )
  /ilkiv = MEAN( K3503 )
  /nahair = MEAN( K3504 )
  /huum = MEAN( K3505 )
  /uhka = MEAN( K3506 )
  /vahingon = MEAN( K3507 )
  /hpiha =MEAN( K3508 )  
  /tyyturv = MEDIAN( K1304) /* eos skaalan ulkopuolisena
  /tyypuh = MEDIAN( K1306) /* eos skaalan ulkopuolisena
    /roskam =MEDIAN( K3601 ) /* eos skaalan ulkopuolisena
  /juopm = MEDIAN( K3602 ) /* eos skaalan ulkopuolisena
  /ilkivm = MEDIAN( K3603 ) /* eos skaalan ulkopuolisena
  /nahairm = MEDIAN( K3604 ) /* eos skaalan ulkopuolisena
  /huumm = MEDIAN( K3605 ) /* eos skaalan ulkopuolisena
  /uhkam = MEDIAN( K3606 ) /* eos skaalan ulkopuolisena
  /vahingonm = MEDIAN( K3607 ) /* eos skaalan ulkopuolisena
  /hpiham =MEDIAN( K3608 )  /* eos skaalan ulkopuolisena
  /skmaleks =MEAN( K5001 )
  /skgraf =MEAN( K5002 )
  /skasiaton =MEAN( K5003 )
  /sktappelu =MEAN( K5004 )
  /skhumala =MEAN( K5005 )
  /skyo =MEAN( K5006 ) 
  /turka =MEAN( K37 ) 
  /turlap =MEAN( K38 ) 
  /taltila =MEAN( K25 ) 
  /talkeh =MEAN( K26 ) 
  /taltoim =MEAN( K27 ).

* weight off.

APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K45b /TARGET VARIABLES= suhde /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K1501 /TARGET VARIABLES= ylpea /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K1502 /TARGET VARIABLES= hapea /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K1503 /TARGET VARIABLES= kaipaus /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K1504 /TARGET VARIABLES= pidan /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4601 /TARGET VARIABLES= juttelu /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4602 /TARGET VARIABLES= kyla /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4603 /TARGET VARIABLES= napu /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4604 /TARGET VARIABLES= ytyo /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4605 /TARGET VARIABLES= jarj /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K47 /TARGET VARIABLES= ulkon /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4801 /TARGET VARIABLES= naut /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4802 /TARGET VARIABLES= nhenki /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4803 /TARGET VARIABLES= luot /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4804 /TARGET VARIABLES= toimeen /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K4805 /TARGET VARIABLES= arvo /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K49 /TARGET VARIABLES= sama /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K37 /TARGET VARIABLES= turka /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= K38 /TARGET VARIABLES= turlap /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K5106	/TARGET VARIABLES= 	hengentarv	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3501	/TARGET VARIABLES= 	roska 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3502	/TARGET VARIABLES= 	juop 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3503	/TARGET VARIABLES= 	ilkiv 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3504	/TARGET VARIABLES= 	nahair	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3505	/TARGET VARIABLES= 	huum 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3506	/TARGET VARIABLES= 	uhka 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3507	/TARGET VARIABLES= 	vahingon 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3508	/TARGET VARIABLES= 	hpiha 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K1304	/TARGET VARIABLES= 	tyyturv 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K1306	/TARGET VARIABLES= 	tyypuh	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3601	/TARGET VARIABLES= 	roskam	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3602	/TARGET VARIABLES= 	juopm 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3603	/TARGET VARIABLES= 	ilkivm	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3604	/TARGET VARIABLES= 	nahairm 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3605	/TARGET VARIABLES= 	huumm 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3606	/TARGET VARIABLES= 	uhkam 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3607	/TARGET VARIABLES= 	vahingonm 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K3608	/TARGET VARIABLES= 	hpiham	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K5001	/TARGET VARIABLES= 	skmaleks	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K5002	/TARGET VARIABLES= 	skgraf 	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K5003	/TARGET VARIABLES= 	skasiaton	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K5004	/TARGET VARIABLES= 	sktappelu	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K5005	/TARGET VARIABLES= 	skhumala	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K5006	/TARGET VARIABLES= 	skyo	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K25	/TARGET VARIABLES= 	taltila	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K26	/TARGET VARIABLES= 	talkeh	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.
APPLY DICTIONARY /FROM * /SOURCE VARIABLES= 	K27	/TARGET VARIABLES= 	taltoim	 /FILEINFO /VARINFO LEVEL ROLE VALLABELS=REPLACE VARLABEL	.

* Poista imputoidut kiintymyksen havainnot von Hippelin mukaisesti. imp_2 jää jotta voi verrata MIDin vaikutusta.

DATASET COPY prefare_jp.
DATASET ACTIVATE prefare_jp.
FILTER OFF.
USE ALL.
SELECT IF (Imputation_ = 0).
EXECUTE.
FILTER OFF.
USE ALL.
SELECT IF (miss_vaste=1).
EXECUTE.
DELETE VARIABLES Imputation_.

SAVE OUTFILE='prefare_jp.sav'
  /COMPRESSED.
DATASET NAME Prefare WINDOW=FRONT.


*tehdään vertailuaineistot.
    USE ALL.
    COMPUTE filter_$=(uniform(1)<=.50).
    VARIABLE LABELS filter_$ 'Approximately 50% of the cases (SAMPLE)'.
    FORMATS filter_$ (f1.0).
    
    DATASET COPY  prefare_jp_filter0.
    DATASET ACTIVATE  prefare_jp_filter0.
    FILTER OFF.
    USE ALL.
    SELECT IF (filter_$ = 0).
    EXECUTE.
    DATASET ACTIVATE  Prefare.
    
    DATASET COPY  prefare_jp_filter1.
    DATASET ACTIVATE  prefare_jp_filter1.
    FILTER OFF.
    USE ALL.
    SELECT IF (filter_$ = 1).
    EXECUTE.
    DATASET ACTIVATE  Prefare.
    FILTER OFF.
    USE ALL.