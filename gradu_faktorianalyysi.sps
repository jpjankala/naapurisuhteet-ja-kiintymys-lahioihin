*graduaineiston eksploratiivinen faktorianalyysi.
*faktoripisteiden laskeminen ja viimeisiä muuttujamuunnoksia ja standardointeja imputoidulla aineistolla. multikollineaarisuuden tarkastaminen.

cd 'C:\Gradu'.
GET FILE='prefare_jp.sav'.
DATASET NAME Prefare WINDOW=FRONT.
Dataset activate Prefare.

weight by wadj1.

* Faktoroidaan vaste.
* huomattavan matala alpha, huomattavan hyvä jos poistaa häpeän. harkittiin häpeän tiputtamista, mutta säilytettiin mukana.

     FACTOR
      /VARIABLES ylpea hapea kaipaus pidan
      /MISSING LISTWISE 
      /ANALYSIS ylpea hapea kaipaus pidan
      /PRINT INITIAL KMO REPR EXTRACTION ROTATION
      /PLOT EIGEN
      /FORMAT BLANK(.00)
      /CRITERIA FACTORS(1) ITERATE(25)
      /EXTRACTION PAF
      /CRITERIA ITERATE(25)
      /ROTATION varimax          
      /SAVE REG(ALL).

	RELIABILITY
	  /VARIABLES= ylpea hapea kaipaus pidan
	  /SCALE('ALL VARIABLES') ALL
	  /MODEL=ALPHA
	  /STATISTICS=SCALE
	  /SUMMARY=TOTAL.

	FREQUENCIES    FAC1_1 /HISTOGRAM.
	
* Diskretoidaan vaste. Tämä oli tulosten luotettavuutta heikentävä ajatteluvirhe, sillä vastetta olisi sellaisenaan voinut käyttää lineaarisessa mallissa.

	RANK VARIABLES=FAC1_1 /ntiles(4).
	recode NFAC1_1 (1=1) (else=0) into kiin2.
	variable labels kiin2 'Kiintymys asuinalueeseen'.
	value labels kiin2 0 'Ei erityisen kiintynyt' 1 'Kiintynein neljännes'.
	exe.
	variable role /TARGET kiin2.

	MEANS TABLES= fac1_1
	  /CELLS=COUNT MEAN STDDEV RANGE min max SKEW KURT.
	MEANS TABLES= fac1_1 by kiin2
	  /CELLS=COUNT MEAN STDDEV RANGE min max SKEW KURT.
	desc kiin2.
	GRAPH
	  /HISTOGRAM=FAC1_1
	  /PANEL ROWVAR=kiin2 ROWOP=CROSS.
  
* Kokeiltiin faktoroida lähiötason taustafaktorit (työttömät korkeakoulutetut) (väki tulot asumisväljyys), mutta toimi lopulta huonosti.
* kmo mediocre 0,65. kommu >0,5, lataukset rotatoidut >0,7.

	 *FACTOR
	  /VARIABLES ruutu_tyotonpertyo ruutu_korkperkaikki ruutu_keskiostov ruutu_asval ruutu_vaki
	  /MISSING LISTWISE 
	  /ANALYSIS  ruutu_tyotonpertyo ruutu_korkperkaikki ruutu_keskiostov ruutu_asval ruutu_vaki
	  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
	  /PLOT EIGEN
	  /FORMAT BLANK(.00)
	  /CRITERIA FACTORS(2) ITERATE(100)
	  /EXTRACTION PAF
	  /CRITERIA ITERATE(25)
	  /ROTATION varimax         
	  /SAVE REG(ALL).

	*rename variables (FAC1_2 FAC2_2 = Ruutueko Ruutusosio).
	*variable labels Ruutueko 'Lähiön talous ja väkimäärä' Ruutusosio 'Lähiön korkeakoulutus ja työttömyys'.

* Kokeiltiin faktoroida yksilötason taustamuuttujat, mutta näyttävät kuvaavan eri ulottuvuuksia.

	 * FACTOR
	  /VARIABLES koulutus taltila talkeh taltoim
	  /MISSING LISTWISE 
	  /ANALYSIS  taltila talkeh taltoim
	  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
	  /PLOT EIGEN
	  /FORMAT BLANK(.00)
	  /CRITERIA FACTORS(1) ITERATE(100)
	  /EXTRACTION PAF
	  /CRITERIA ITERATE(25)
	  /ROTATION varimax.

* Luokitellaan taloudellinen tila.

	recode taltila (lowest thru 2.99=0) (3=1) (3.01 thru HIGHEST=2).
	variable labels taltila 'Kotitalouden taloudellinen tilanne'.
	value labels taltila 0 'Hyvä taloudellinen tilanne' 1 'Kohtalainen taloudellinen tilanne' 2 'Huono taloudellinen tilanne'.
	exe.

* Tutkimuskysymys 1 faktorianalyysi ja faktoripisteet.
* tarkastelu suurella määrällä muuttujia.
* asaika, sukualue, ysterotuscat, sama pois koska kommunaliteetti alle 0,2. ystaluecat kom lähellä 0,2. osan voinee tuoda erillisinä. ystaluecat ja ysterotus sisältävät missing.

 FACTOR
  /VARIABLES  asaika juttelu kyla napu ytyo jarj ulkon naut nhenki luot toimeen arvo sama hengentarv ystaluecat ysterotuscat sukualue
  /MISSING LISTWISE 
  /ANALYSIS  asaika juttelu kyla napu ytyo jarj ulkon naut nhenki luot toimeen arvo sama hengentarv ystaluecat ysterotuscat sukualue
  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
  /PLOT EIGEN
  /FORMAT BLANK(.00)
  /CRITERIA FACTORS(3) ITERATE(25)
  /EXTRACTION PAF
  /CRITERIA ITERATE(25)
  /ROTATION varimax.

* Lasketaan faktoripisteet ilman imputoimattomia ja matalan kommunaliteetin muuttujia

	 FACTOR
	  /VARIABLES asaika juttelu kyla napu ytyo jarj ulkon naut nhenki luot toimeen arvo sama hengentarv sukualue
	  /MISSING LISTWISE 
	  /ANALYSIS  juttelu kyla napu ytyo jarj ulkon naut nhenki luot toimeen arvo hengentarv 
	  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
	  /PLOT EIGEN
	  /FORMAT BLANK(.00)
	  /CRITERIA FACTORS(2) ITERATE(100)
	  /EXTRACTION PAF
	  /CRITERIA ITERATE(25)
	  /ROTATION varimax     
	  /SAVE REG(ALL).

	RELIABILITY
	  /VARIABLES=  juttelu kyla napu ytyo jarj ulkon naut nhenki luot toimeen arvo hengentarv 
	  /SCALE('ALL VARIABLES') ALL
	  /MODEL=ALPHA
	  /STATISTICS=SCALE
	  /SUMMARY=TOTAL.
		RELIABILITY
		  /VARIABLES=  juttelu kyla napu ytyo jarj ulkon hengentarv 
		  /SCALE('ALL VARIABLES') ALL
		  /MODEL=ALPHA
		  /STATISTICS=SCALE
		  /SUMMARY=TOTAL.
		RELIABILITY
		  /VARIABLES=  naut nhenki luot toimeen arvo 
		  /SCALE('ALL VARIABLES') ALL
		  /MODEL=ALPHA
		  /STATISTICS=SCALE
		  /SUMMARY=TOTAL.

	rename variables (FAC1_2 FAC2_2 = tuttuus yhenki).
	variable labels tuttuus 'Kanssakäyminen (vuorovaikutus, tunteminen, tärkeys)' yhenki 'Yhteishenki'.

* Tutkimuskysymys 2 faktorianalyysi ja faktoripisteet. 
*tarkastelu suurella määrällä muuttujia.
*arvo latautuu heikosti kahdelle faktorille, mutta tuskin vaikuttaa paljoa.
*turka turlap ystaluecat ysterotuscat tyyturv tyypuh sisältävät puuttuvia.

 FACTOR
  /VARIABLES juttelu kyla napu ytyo jarj ulkon naut nhenki luot toimeen arvo hengentarv ystaluecat ysterotuscat sukualue
  roska juop ilkiv nahair huum uhka vahingon hpiha tyyturv tyypuh skmaleks skgraf skasiaton sktappelu skhumala skyo turka turlap
  /MISSING LISTWISE 
  /ANALYSIS  juttelu kyla napu ytyo jarj ulkon naut nhenki luot arvo hengentarv 
  roska juop ilkiv nahair huum uhka vahingon hpiha tyyturv tyypuh skmaleks skgraf skasiaton sktappelu skhumala skyo 
  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
  /PLOT EIGEN
  /FORMAT BLANK(.00)
  /CRITERIA FACTORS(3) ITERATE(25)
  /EXTRACTION PAF
  /CRITERIA ITERATE(25)
  /ROTATION varimax .

* Lasketaan faktoripisteet ilman tyytyväisyyttä ja imputoimattomia.

	FACTOR
	  /VARIABLES juttelu kyla napu ytyo jarj ulkon naut nhenki luot toimeen arvo hengentarv  sukualue
	  roska juop ilkiv nahair huum uhka vahingon hpiha skmaleks skgraf skasiaton sktappelu skhumala skyo 
	  /MISSING LISTWISE 
	  /ANALYSIS  juttelu kyla napu ytyo jarj ulkon naut nhenki luot arvo hengentarv 
	  roska juop ilkiv nahair huum uhka vahingon hpiha  skmaleks skgraf skasiaton sktappelu skhumala skyo 
	  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
	  /PLOT EIGEN
	  /FORMAT BLANK(.00)
	  /CRITERIA FACTORS(3) ITERATE(25)
	  /EXTRACTION PAF
	  /CRITERIA ITERATE(25)  
	  /ROTATION varimax 
	  /SAVE REG(ALL).

	rename variables (FAC1_2 FAC2_2 FAC3_2 = epajar naap sosk).
	variable labels epajar 'Epäjärjestys' naap 'Naapuruus (tuttuus ja yhteishenki)' sosk 'Luottamus sosiaaliseen kontrolliin'.

	RELIABILITY
	  /VARIABLES= juttelu kyla napu ytyo jarj ulkon naut nhenki luot arvo hengentarv 
	   roska juop ilkiv nahair huum uhka vahingon hpiha  skmaleks skgraf skasiaton sktappelu skhumala skyo 
	  /SCALE('ALL VARIABLES') ALL
	  /MODEL=ALPHA
	  /STATISTICS=SCALE
	  /SUMMARY=TOTAL.
		RELIABILITY
		  /VARIABLES= juttelu kyla napu ytyo jarj ulkon naut nhenki luot arvo hengentarv 
		  /SCALE('ALL VARIABLES') ALL
		  /MODEL=ALPHA
		  /STATISTICS=SCALE
		  /SUMMARY=TOTAL.
		RELIABILITY
		  /VARIABLES=roska juop ilkiv nahair huum uhka vahingon hpiha  
		  /SCALE('ALL VARIABLES') ALL
		  /MODEL=ALPHA
		  /STATISTICS=SCALE
		  /SUMMARY=TOTAL.
		RELIABILITY
		  /VARIABLES=  skmaleks skgraf skasiaton sktappelu skhumala skyo 
		  /SCALE('ALL VARIABLES') ALL
		  /MODEL=ALPHA
		  /STATISTICS=SCALE
		  /SUMMARY=TOTAL.
		  
		  
		  
		  
* Muuttujamuunnoksia.

* tarkasta skaalat ja käännä siten, että enemmän on isompi.

	means    tuttuus by juttelu.
	means yhenki by pidan.
	means epajar by ilkiv.
	means naap by juttelu.
	means sosk by sktappelu.

	compute ituttuus =0.
	compute iyhenki =0.
	compute inaap =0.
	formats ituttuus iyhenki inaap (f11.5).
	variable width ituttuus iyhenki inaap (13).
	variable level ituttuus iyhenki inaap (SCALE    ).
	exe.
	compute ituttuus = tuttuus*(-1).
	compute iyhenki = yhenki*(-1).
	compute inaap = naap*(-1).
	exe.
	GRAPH  /SCATTERPLOT(BIVAR)=inaap WITH naap.
	GRAPH  /SCATTERPLOT(BIVAR)=iyhenki WITH yhenki.
	GRAPH  /SCATTERPLOT(BIVAR)=ituttuus WITH tuttuus.
	DELETE VARIABLES    tuttuus yhenki naap.
	RENAME VARIABLES   ( ituttuus iyhenki inaap = tuttuus yhenki naap).
	variable labels tuttuus 'Kanssakäyminen (vuorovaikutus, tunteminen, tärkeys)' yhenki 'Yhteishenki' naap 'Naapuruus (tuttuus ja yhteishenki)' .

* perusyhteys.

	CROSSTABS
	  /TABLES=suhde BY kiin2
	  /FORMAT=AVALUE TABLES
	  /STATISTICS=CHISQ 
	  /CELLS=COUNT COLUMN 
	  /COUNT ROUND CELL.

* standardoidaan alueelliset selittäjät, katsotaan vif, todetaan että pitäydytään työttömyydessä.

	desc ruutu_tyotonpertyo  /save.
	
* Standardoidaan ikä (harkittiin grand mean centered).

	*ikä gm.
	*    DESCRIPTIVES VARIABLES=ruutu_vaki ruutu_tyotonpertyo ruutu_keskiostov ruutu_perusperkaikki ruutu_korkperkaikki ruutu_asval ika
		  /STATISTICS=MEAN STDDEV MIN MAX.
	*    compute gm_ika = ika - 50.2171388812446.
	*    EXECUTE    .
	*    variable labels gm_ika 'Ikä'.

	desc ika /save.

* Tarkastetaan faktorien välinen multikollineaarisuus.

	weight off.
	*tk1. ystpaikal ystaluecat puuttuu koska ongelmana missing=0-kysymys. muutto puuttuu.
	REGRESSION
	  /MISSING LISTWISE
	  /REGWGT=wadj1
	  /STATISTICS COEFF OUTS R ANOVA COLLIN TOL
	  /CRITERIA=PIN(.05) POUT(.10)
	  /NOORIGIN 
	  /DEPENDENT kiin2
	  /METHOD=ENTER Zika taltila hallinta sukualue Zruutu_tyotonpertyo
				  suhde
				  tuttuus yhenki.
	*tk2.
	REGRESSION
	  /MISSING LISTWISE
	  /REGWGT=wadj1
	  /STATISTICS COEFF OUTS R ANOVA COLLIN TOL
	  /CRITERIA=PIN(.05) POUT(.10)
	  /NOORIGIN 
	  /DEPENDENT kiin2
	  /METHOD=ENTER Zika taltila hallinta sukualue Zruutu_tyotonpertyo
					  epajar
					  suhde
					  naap sosk.

	*lineaarinen vaste.
	REGRESSION
	  /MISSING LISTWISE
	  /REGWGT=wadj1
	  /STATISTICS COEFF OUTS R ANOVA COLLIN TOL
	  /CRITERIA=PIN(.05) POUT(.10)
	  /NOORIGIN 
	  /DEPENDENT fac1_1
	  /METHOD=ENTER Zika taltila hallinta sukualue Zruutu_tyotonpertyo
					  epajar
					  suhde
					  naap sosk.

	weight by wadj1.

	*suhde ja naap 1,6 vif. suhde*naap R2 0,32 suhde*tuttuus 0,26.
	MEANS TABLES= naap tuttuus yhenki  by suhde
	  /CELLS=MEAN COUNT STDDEV KURT SKEW
	  /STATISTICS anova LINEARITY.


	*ONEWAY ruutueko by kiin2 /* varianssien homogeenisuus p>0,05
	  /STATISTICS DESCRIPTIVES EFFECTS HOMOGENEITY 
	  /PLOT MEANS.

	*EXAMINE VARIABLES=epajar2 BY kiin2
	  /PLOT=BOXPLOT
	  /STATISTICS=NONE
	  /NOTOTAL.

*korjataan suvun koodaus.

	recode sukualue (0=1) (1=0).
		variable labels sukualue 'Asuuko sukua alueella'.
		value labels sukualue 1 'Sukua alueella' 0 'Ei sukua alueella tai puuttuu'.
		variable level sukualue (NOMINAL).

weight off.