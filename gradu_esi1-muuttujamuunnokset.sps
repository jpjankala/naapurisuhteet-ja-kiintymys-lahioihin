*graduaineiston esikäsittely 1: muuttujamuunnokset.

cd 'C:\Gradu'.
GET FILE='prefare_metalla_lokakuu2015_peruspainot_pre1_jp.sav'.
ALTER TYPE ALL(A=AMIN).
DATASET NAME Prefare WINDOW=FRONT.

compute JP______ = 0.
EXECUTE.
VARIABLE ROLE /NONE JP______.


weight by wadj1. /* Kaikki muuttujamuunnokset ja analyysit katokorjatulla analyysipainolla. Pois käytöstä vain jos menetelmässä itsessään painokertoimen paikka.

* Tarkastetaan puuttuneisuus jos deletointi listwise. sis kaikki paitsi ystävien määrä alueella.

	COMPUTE valid = 1.
	IF (NVALID( K45	, 
	K1501 	, 
	K1502 	, 
	K1503 	, 
	K1504 	, 
	K4601 	, 
	K4602 	, 
	K4603 	, 
	K4604 	, 
	K4605 	, 
	K47 	, 
	K4801 	, 
	K4802 	, 
	K4803  	, 
	K4804 	, 
	K4805  	, 
	K49  	, 
	K5106	, 
	K3501 	, 
	K3502 	, 
	K3503 	, 
	K3504 	, 
	K3505 	, 
	K3506 	, 
	K3507 	, 
	K3508   	, 
	K1304	, 
	K1306	, 
	K5001 	, 
	K5002 	, 
	K5003 	, 
	K5004 	, 
	K5005 	, 
	K5006  	
	 ) <34) valid = 0. /* 
	EXECUTE.
	VARIABLE LABELS valid 'Puuttuneisuus'.
	VALUE LABELS valid 0 'Puuttuvia' 1 'Ei puuttuvia'.
	fre valid.
	
* menetettäisiin 14,7% aineistosta. talletetaan vertailua varten manuaalisesti prefare_jp_listwise.sav .

* määritellään ruutukanta-aineistosta alueelliset selittäjät ja keskitetään painotetun keskiarvon ympärille.
    
    VARIABLE LABELS
    Lähiö 'Lähiö 250mx250m ruudun mukaan'
    Pt_vakiy_09 'Väestömäärä'
    Pt_tyovy_09 'Työvoima'
    Pt_tyott_09 'Työttömät'
    Te_taly_09 'Talouksien lukumäärä'
    Te_laps_09 'Lapsitaloudet (0–17 v.)'
    Tk_ovker_09 'Talouksien keskiostovoima, euroa'
    Ra_asunn_09 'Asuntojen lukumäärä'
    Ra_kt_as_09 'Kerrostaloasuntoja' .
    
    compute ruutu_vaki = Pt_vakiy_09.
    compute ruutu_tyotonpertyo = Pt_tyott_09 / Pt_tyovy_09.
    compute ruutu_keskiostov = Tk_ovker_09.
    compute ruutu_perusperkaikki = Ko_perus_09 / Ko_ika18y_09.
    compute ruutu_korkperkaikki = (Ko_al_kork_09 + Ko_yl_kork_09) / Ko_ika18y_09.
    EXECUTE.
    
    DESCRIPTIVES VARIABLES=ruutu_vaki ruutu_tyotonpertyo ruutu_keskiostov ruutu_perusperkaikki ruutu_korkperkaikki
      /STATISTICS=MEAN STDDEV MIN MAX.
    
    VARIABLE LABELS  ruutu_vaki 'Väestömäärä ruudussa 2008' /* otannassa isot ja pienet kaupungit. ohitan kaupunkitason mutta otan huomioon paikallisen tiheyden.
     ruutu_tyotonpertyo 'Työttömien osuus ruudun työvoimasta 2008' /* otannassa korkea ja matala työttömyys.
     ruutu_keskiostov 'Talouksien keskiostovoima ruudussa 2008'
     ruutu_perusperkaikki 'Perusasteen koulutettujen osuus kaikista 2008'
     ruutu_korkperkaikki 'Korkeakoulutettujen osuus kaikista 2008'.

    *asumisväljyydestä pisteet pilkuiksi ja numeraaliseksi.
    string ruutu_asval (a5).
    recode  Ra_as_valj_09 (else = copy) into ruutu_asval .
    exe.
    compute ruutu_asval  = replace(ruutu_asval ,'.',',').
    exe.
    alter type ruutu_asval  (f3.1).
    variable level ruutu_asval (scale).
    fre ruutu_asval /histogram.
        VARIABLE LABELS  ruutu_asval 'Asumisväljyys ruudussa 2008'.


* määritellään ikä korvaamalla virheelliset iät ruutukanta-aineiston tiedoilla ja vähentämällä 2013:sta.
* Korvasin virheellisesti ilmoitetut syntymävuodet paikkatietoaineiston otostiedoilla. Ainoastaan kaksi korvausta olivat kyseenalaisia: 1937 korvaaminen 1939:llä ja 1934 1974:llä.
    
    IF (K02<1938) ikafilter = (K02).	
    IF (K02>1995) ikafilter = (K02).	
    EXECUTE.
    
    CROSSTABS Syntvuosi by ikafilter /CELLS=column.
    
    compute ikatemp = K02.
    if (ikafilter>0) ikatemp = Syntvuosi. /* jos ikä tarkasteluvälin 18–75 ulkopuolella, niin paikkatietoaineistosta.
    if (sysmis(K02)) ikatemp = Syntvuosi. /* jos ikä missing niin paikkatietoaineistosta.
    EXECUTE.
    
    fre ikatemp.
    
    compute ika = 2013-ikatemp.
    EXECUTE.
    variable labels ika 'Ikä vuonna 2013'.
    
    DESCRIPTIVES VARIABLES=ika
      /STATISTICS=MEAN STDDEV MIN MAX.
    
* puuttuva sukupuoli paikkatietoaineistosta, eli valistunut arvaus. 
    
    compute sp = K01.
    if (sysmis(K01)) sp = Sukupuoli.
    recode sp (2=0) (1=1).
    execute.
    variable labels sp 'Sukupuoli ref.nainen'.
    value labels sp 0 'Nainen' 1 'Mies'.
    VARIABLE LEVEL sp (NOMINAL).

    fre sp.
    crosstabs K01 by sp /cells=column.
    
* koulutusaste tilastokeskuksen mukaan. pohjatasona puuttuva/muu yleissivistävästä ja ammatillisesta puuttuviin, sitten yleissivistävän mukaan paikoilleen, lopuksi päälle ammatillinen ja korkea.

    IF (sysmis(K06)) koulutus=3. /* pohjataso puuttuvasta yleissivistävästä, siirtyvät mikäli muuta koulutusta.
    IF (sysmis(K07)) koulutus=3. /* puuttuva ammatillinen, siirtyvät mikäli muuta koulutusta.
    IF (K07=1) koulutus=3. /* ei ammatillista koulutusta puuttuviin.
    IF (K06=4) koulutus=3. /* muu yleissivistävä puuttuviin. ellei tämä viittaa yksityiskouluihin, jolloin oikeampi olisi perusasteen yleissivistävä?.
    IF (K06<=2) koulutus=0. /* kansa, kansalais, perus, keski perusasteeseen.
    IF (K06=3) koulutus=1. /* ylioppilas keskiasteeseen.
    IF (K07>=2) koulutus=1. /* kaikki ammatillinen paitsi ei koulutusta keskiasteeseen. korkea-aste siirtyy väliaikaisesti mukana.
    IF (K07>=6) koulutus=2. /* amk, korkeakoulu, yliopistot korkea-asteeseen.
    EXECUTE.
    VARIABLE LABELS koulutus 'Koulutusaste ref.perusaste'. 
    VARIABLE LEVEL koulutus (NOMINAL).
    VALUE LABELS koulutus
    0 'Perusasteen yleissivistävä'
    1 'Keskiasteen yleissivistävä tai ammatillinen'
    2 'Korkea-aste'
    3 'Muu tai puuttuu' .
    
    fre koulutus.  
    CROSSTABS K06 by koulutus /CELLS=column.
    CROSSTABS K07 by koulutus /CELLS=column.
    CROSSTABS K06 by K07 /CELLS=column.
    
* yhdistetään hallintasuhteen pienet luokat ja missing muihin.
    
    recode K08 (1=0) (2=1) (3=2) (else=3) into hallinta.
    execute.
    variable labels hallinta 'Hallintasuhde ref.omistus'.
    value labels hallinta 0 'Omistusasunto' 1 'Yksityinen vuokra-asunto' 2 'Kunnallinen tai muu ei-yksityinen vuokra-asunto' 3 'Muu tai puuttuu'.
    variable level hallinta (NOMINAL).
    
    CROSSTABS K08 by hallinta /CELLS=column.
    
* yhdistetään talotyypin pienet luokat ja missing muihin.
    
    recode K09 (1=1) (2=0) (else=2) into talo.
    execute.
    variable labels talo 'Talotyyppi ref.pienkerrostalo'.
    value labels talo 0 'Pienkerrostalo 2–4 krs' 1 'Kerrostalo 5+ krs' 2 'Muu tai puuttuu'.
    variable level talo (NOMINAL).
    
    CROSSTABS K09 by talo /CELLS=column.
    
* yhdistetään kotitalouden tyypin pienet luokat ja missing muihin, eli asuu vanhempiensa kanssa ja asuu kimppakämpässä.
    
    compute koti = K04 -1.
    recode koti (5=4) (sysmis=4).
    execute.
    variable labels koti 'Kotitalouden muoto ref.asuu yksin'.
    value labels koti 0 'Asun yksin' 1 'Asun puolisoni kanssa' 2 'Asun puolisoni ja lasten kanssa' 3 'Asun yksin lasten kanssa' 4 'Muu tai puuttuu'.
    variable level koti (nominal).
    
    fre koti.
    crosstabs K04 by koti /cells=column.
    
* koodataan asumisajan puuttuvat esiin.
    
    recode K11B (1=3) (2=2) (3=1) (4=0) (sysmis=4) into asaika.
    execute.
    VARIABLE LEVEL asaika (NOMINAL).
    variable labels asaika 'Asunut alueella ref.yli 6 v.'.
    VALUE LABELS asaika 3 'Alle 1 v.' 2 '1–3 v.' 1 '3–6 v.' 0 'Yli 6 v.' 4 'Puuttuu'.
    
    fre asaika.
    crosstabs K11B by asaika /cells=column.
    
* erotetaan työttömyyttä nyt tai viimeisen kolmen vuoden aikana kokeneet plus eläkeläiset. jätetään kotivanhemmat korostamatta, koska tulee jo kotitalouden muodon kautta.
    
    crosstabs K20A by K22 /cells=column.
    
    IF (K20A=7) tyo = 2. /* eläkeläinen eläkeläisiin.
    IF (K22=1) tyo = 1. /* työttömyyttä 3v aikana työttömiin, tähän siirtyvät nykyään eläkkeellä olevat, joilla työttömyyttä 3v sisällä.
    IF (K20A=6) tyo = 1. /* työtön nyt työttömiin.
    recode tyo (sysmis=0) (else=copy). /* referenssiryhmänä muut.
    execute.
    variable labels tyo 'Työttömyys ref.töissä, opiskelee, kotivanhempi tai muu'.
    value labels tyo 0 'Muu tai puuttuu' 1 'Työtön nyt tai viimeisen kolmen vuoden aikana' 2 'Eläkkeellä'.
    variable level tyo (nominal).
    
    fre tyo.
    crosstabs K20A by tyo /cells=column.
    crosstabs K22 by tyo /cells=column.
    
* muutetaan ystävien määrä numeraaliseksi ja koodataan ranget.
    
    AUTORECODE VARIABLES=K42
      /INTO ystalue2
      /BLANK=MISSING
      /PRINT.
    
    RECODE ystalue2 (3 thru 40=0) (1 thru 2=1) (41=1) (else=sysmis).
    execute.
    VARIABLE LABELS ystalue2 'Asuuko ystäviä alueella ref.asuu'.
    value labels ystalue2 0 'Ystäviä alueella' 1 'Ei ystäviä alueella tai puuttuu'.
    VARIABLE LEVEL ystalue2 (NOMINAL).
    
    CROSSTABS K42 by ystalue2 /CELLS=column.

	* muutetaan ystävien määrä yleensä ja alueella numeraaliseksi. verrataan stringiin pyöristetään erot, muutetaan numeraaliseksi. nollataan jos ei luottamuksellisia ystäviä ylipäänsä, eli rajaa ystaluetta.
		
		string s_yst (a5).
		if (K41=1) s_yst = '0'.
		recode K41B (else = copy) into s_yst.
		exe.
		
		fre s_yst.
		compute s_yst = replace(s_yst,'2-3-4','3').
		compute s_yst = replace(s_yst,'20-30','25').
		compute s_yst = replace(s_yst,'10-15','12,5').
		compute s_yst = replace(s_yst,'10-20','15').
		compute s_yst = replace(s_yst,'10-','10').
		compute s_yst = replace(s_yst,'n.100','100').
		compute s_yst = replace(s_yst,'1-2','1,5').
		compute s_yst = replace(s_yst,'1-3','2').
		compute s_yst = replace(s_yst,'1-5','3').
		compute s_yst = replace(s_yst,'2-3','2,5').
		compute s_yst = replace(s_yst,'2-4','3').
		compute s_yst = replace(s_yst,'2-5','3,5').
		compute s_yst = replace(s_yst,'3-4','3,5').
		compute s_yst = replace(s_yst,'3-5','4').
		compute s_yst = replace(s_yst,'3-6','4,5').
		compute s_yst = replace(s_yst,'3-7','5').
		compute s_yst = replace(s_yst,'4-10','7').
		compute s_yst = replace(s_yst,'4-5','4,5').
		compute s_yst = replace(s_yst,'4-6','5').
		compute s_yst = replace(s_yst,'4-8','6').
		compute s_yst = replace(s_yst,'5-10','7,5').
		compute s_yst = replace(s_yst,'5-6','5,5').
		compute s_yst = replace(s_yst,'5-7','6').
		compute s_yst = replace(s_yst,'5-8','6,5').
		compute s_yst = replace(s_yst,'6-7','6,5').
		compute s_yst = replace(s_yst,'6-8','7').
		compute s_yst = replace(s_yst,'7-8','7,5').
		compute s_yst = replace(s_yst,'7-9','8').
		exe.
		fre s_yst.
		
		string yst (a5).
		recode s_yst (else = copy) into yst.
		exe.
		alter type yst (f3.1).
		variable level yst (scale).
		fre yst /histogram.
		* muunna 21 alkaen yhteen luokkaan?.
		
		string s_ystalue (a5).
		if (K41=1) s_ystalue = '0'.
		recode K42 (else = copy) into s_ystalue.
		EXECUTE.
		
		fre s_ystalue.
		compute s_ystalue = replace(s_ystalue,'10-15','12,5').
		compute s_ystalue = replace(s_ystalue,'6-7','6,5').
		compute s_ystalue = replace(s_ystalue,'5-6','5,5').
		compute s_ystalue = replace(s_ystalue,'5-10','7,5').
		compute s_ystalue = replace(s_ystalue,'4-5','4,5').
		compute s_ystalue = replace(s_ystalue,'4-10','7').
		compute s_ystalue = replace(s_ystalue,'3-7','5').
		compute s_ystalue = replace(s_ystalue,'3-5','4').
		compute s_ystalue = replace(s_ystalue,'3-4','3,5').
		compute s_ystalue = replace(s_ystalue,'2-6','4').
		compute s_ystalue = replace(s_ystalue,'2-5','3,5').
		compute s_ystalue = replace(s_ystalue,'2-4','3').
		compute s_ystalue = replace(s_ystalue,'2-3','2,5').
		compute s_ystalue = replace(s_ystalue,'1-2','1,5').
		compute s_ystalue = replace(s_ystalue,'-','0').
		exe.
		fre s_ystalue.

		string ystalue (a5).
		recode s_ystalue (else = copy) into ystalue.
		alter type ystalue (f2.1).
		variable level ystalue (scale).
		fre ystalue /histogram.
		
		recode ystalue (0=1) (1=2) (1.5 thru 2=3) (2.5 thru 3=4) (3.5 thru 4=5) (4.5 thru 5=6) (5.5 thru 9=7) (10 thru 14=8) (15 thru highest=9) (else=sysmis) into ystaluecat.
		VARIABLE LEVEL ystaluecat (ORDINAL    ).
		exe.
		fre ystaluecat.
		CROSSTABS ystaluecat by ystalue2. 
	
	* Kokeillaan laskea alueella asuvien ystävien osuus kaikista ystävistä, mutta lomakkeen muotoilu ja puuttuneisuus estävät käytön.
	
		compute ysterotus = (ystalue-yst).
		FREQUENCIES    ysterotus /HISTOGRAM    .
		recode ysterotus (lowest thru 0=1) (0.1 thru HIGHEST=2) into ystpaikal.
		VALUE LABELS    ystpaikal 1 'Enemmän luottamuksellisia ystäviä alueen ulkopuolella kuin ystäviä alueella' 2 'Enemmän ystäviä alueella kuin luottamuksellisia ystäviä alueen ulkopuolella'.
		VARIABLE LABELS ystpaikal 'Ystävien paikallisuus'.
		EXECUTE.
		FREQUENCIES    ystpaikal.
			CROSSTABS    K45 by ystpaikal /cells column.
		
		recode ysterotus (lowest thru -5=1) (-4.9 thru -0.1=2) (0=3) (0.1 thru HIGHEST    =4) into ysterotuscat.
		VALUE LABELS    ysterotuscat 1 'Yli viisi luottamuksellista ystävää enemmän ulkopuolella' 2 'Enemmän luottamuksellisia ystäviä ulkopuolella' 
		3 'Yhtä monta ystävää alueella kuin luottamuksellisia ulkopuolella' 4 'Enemmän ystäviä alueella kuin luottamuksellisia ulkopuolella'.
		VARIABLE LABELS ysterotuscat 'Ystävien paikallisuus'.
		EXECUTE.
		exe.
		*ei varmaan käyttistä sit kuitenkaan.
    
* supistetaan suku.
    
    recode K44 (1 thru 2=0) (else=1) into sukualue.
    execute.
    variable labels sukualue 'Asuuko sukua alueella ref.asuu'.
    value labels sukualue 0 'Sukua alueella' 1 'Ei sukua alueella tai puuttuu'.
    variable level sukualue (NOMINAL).

    fre sukualue.
    crosstabs K44 by ystalue /cells=column.

* lasketaan halu muuttaa asuinalueelta pois.

    compute muutto = 0.
    IF ( K17 = 3) muutto = 1.
    IF ( K18 >= 3) muutto = 1.
    execute.
    variable labels muutto 'Halu muuttaa pois alueelta eri syistä ref.ei halua'.
    value labels muutto 0 'En halua muuttaa alueelta' 1 'Haluan muuttaa alueelta'.
    variable level muutto (NOMINAL).

    fre muutto.
    crosstabs K17 by muutto /cells=column.
    crosstabs K18 by muutto /cells=column.