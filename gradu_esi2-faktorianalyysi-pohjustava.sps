*graduaineiston esikäsittely 2: varmistetaan faktorianalyysilla muuttujien käyttöä.

cd 'C:\Gradu'.
GET FILE='prefare_metalla_lokakuu2015_peruspainot_pre1_jp.sav'.
ALTER TYPE ALL(A=AMIN).
DATASET NAME Prefare WINDOW=FRONT.

* ML olettaa normaalijakauman, mitä näillä ei todellakaan ole, mutta sietää hyvin poikkeamia. tarkastetaan vinous<2 huipukkuus<7 jotta ollaan simulaatiokokeiden raja-arvojen alapuolella.
* tyytyväisyys-kysymykset ongelmallisia molempien suhteen (mutta jätän analyysin kuluessa pois). osa epäjärjestyksen muutoksesta ongelmallista ~2 vinous.

FREQUENCIES VARIABLES=K45 K1501 k1502 K1503 K1504 
          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49
          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006
  /STATISTICS=SKEWNESS SESKEW KURTOSIS SEKURT
  /ORDER=ANALYSIS.

 * FA vasteella, rajattuna tutkys1. ei sis pääselittäjää.
FACTOR
  /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
  /MISSING LISTWISE 
  /ANALYSIS  K1501 k1502 K1503 K1504 
  K1303 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue
  K5103 K5106 
  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
  /PLOT EIGEN
  /FORMAT BLANK(.00)
  /CRITERIA FACTORS(3) ITERATE(25)
  /EXTRACTION PAF
  /CRITERIA ITERATE(25)
  /ROTATION varimax.
                * FA vasteella, rajattuna tutkys1. ei sis pääselittäjää. vertailun vuoksi ML.
                FACTOR
                  /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
                  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki, 
                  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
                  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
                  /MISSING LISTWISE 
                  /ANALYSIS  K1501 k1502 K1503 K1504 
                  K1303 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue
                  K5103 K5106 
                  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
                  /PLOT EIGEN
                  /FORMAT BLANK(.00)
                  /CRITERIA FACTORS(3) ITERATE(25)
                  /EXTRACTION ML
                  /CRITERIA ITERATE(25)
                  /ROTATION varimax.
         * FA vasteella, rajattuna tutkys1. ei sis pääselittäjää. neljä faktoria. tässä näkyy kyläily omana, pienenä faktorinaan, mutta sen rinnalla ystävät ja sukulaiset varsin pienellä kommunaliteetilla, joten faktori ei ehkä perusteltua.
                FACTOR
                  /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
                  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
                  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
                  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
                  /MISSING LISTWISE 
                  /ANALYSIS  K1501 k1502 K1503 K1504 
                  K1303 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue
                  K5103 K5106 
                  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
                  /PLOT EIGEN
                  /FORMAT BLANK(.00)
                  /CRITERIA FACTORS(4) ITERATE(25)
                  /EXTRACTION PAF
                  /CRITERIA ITERATE(25)
                  /ROTATION varimax.
    
    * FA vasteella, rajattuna tutkys1. ei sis pääselittäjää. pois latauksen ja kommunaliteetin takia muuttujia.
    FACTOR
      /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
      K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki, 
      K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
      K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
      /MISSING LISTWISE 
      /ANALYSIS  K1501 k1502 K1503 K1504 
      K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 muutto
      K5106 
      /PRINT INITIAL KMO REPR EXTRACTION ROTATION
      /PLOT EIGEN
      /FORMAT BLANK(.00)
      /CRITERIA FACTORS(3) ITERATE(25)
      /EXTRACTION PAF
      /CRITERIA ITERATE(25)
      /ROTATION varimax.
    
            * FA vasteella, rajattuna tutkys1. ei sis pääselittäjää. edellinen on jo suht käyttökelpoinen, mutta sisältää vielä suuria poikkeamia faktorin sisäisten latausten ja kommunaliteettien välillä..
            FACTOR
              /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
              K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki, 
              K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
              K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
              /MISSING LISTWISE 
              /ANALYSIS  K1501 K1503 K1504 
              K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 muutto
              K5106 
              /PRINT INITIAL KMO REPR EXTRACTION ROTATION
              /PLOT EIGEN
              /FORMAT BLANK(.00)
              /CRITERIA FACTORS(3) ITERATE(25)
              /EXTRACTION PAF
              /CRITERIA ITERATE(25)
              /ROTATION varimax.    

                    * FA vasteella, rajattuna tutkys1. ei sis pääselittäjää. katsotaan ilman viimeisiä kahta heikohkoa kommunaliteettia (lähempänä 0,2 kuin 0,3): poistuu kyläily ja yhteishengen tärkeys, ehkä vuorovaikutusfaktori paranee.
                    FACTOR
                      /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
                      K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki, 
                      K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
                      K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
                      /MISSING LISTWISE 
                      /ANALYSIS  K1501 K1503 K1504 
                      K4601 K4603 K4604 K4605 K47 K4801 K4802 K4803 muutto
                      /PRINT INITIAL KMO REPR EXTRACTION ROTATION
                      /PLOT EIGEN
                      /FORMAT BLANK(.00)
                      /CRITERIA FACTORS(3) ITERATE(25)
                      /EXTRACTION PAF
                      /CRITERIA ITERATE(25)
                      /ROTATION varimax.

* FA tutkys1 ilman vastetta. kohti faktoripisteitä.
FACTOR
  /VARIABLES K45 
  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
  /MISSING LISTWISE 
  /ANALYSIS 
  K1303 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 ystalue sukualue
  K5103 K5106 
  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
  /PLOT EIGEN
  /FORMAT BLANK(.00)
  /CRITERIA FACTORS(3) ITERATE(25)
  /EXTRACTION PAF
  /CRITERIA ITERATE(25)
  /ROTATION varimax.
        
        * FA tutkys1 ilman vastetta. karsitaan huonot, kyläily vielä mukana.
        FACTOR
          /VARIABLES K45 
          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
          K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
          /MISSING LISTWISE 
          /ANALYSIS 
          K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 ystalue sukualue
           K5106 
          /PRINT INITIAL KMO REPR EXTRACTION ROTATION
          /PLOT EIGEN
          /FORMAT BLANK(.00)
          /CRITERIA FACTORS(3) ITERATE(25)
          /EXTRACTION PAF
          /CRITERIA ITERATE(25)
          /ROTATION varimax.
    
        * FA tutkys1 ilman vastetta. kaksi faktoria kolmen sijaan. suku ja ystävät heikkoja.
        FACTOR
          /VARIABLES K45 
          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
          K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
          /MISSING LISTWISE 
          /ANALYSIS 
          K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 ystalue sukualue
           K5106 
          /PRINT INITIAL KMO REPR EXTRACTION ROTATION
          /PLOT EIGEN
          /FORMAT BLANK(.00)
          /CRITERIA FACTORS(2) ITERATE(25)
          /EXTRACTION PAF
          /CRITERIA ITERATE(25)
          /ROTATION varimax.
            
                * FA tutkys1 ilman vastetta. 
                FACTOR
                  /VARIABLES K45 
                  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
                  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
                  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
                  /MISSING LISTWISE 
                  /ANALYSIS 
                  K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805
                   K5106 
                  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
                  /PLOT EIGEN
                  /FORMAT BLANK(.00)
                  /CRITERIA FACTORS(2) ITERATE(25)
                  /EXTRACTION PAF
                  /CRITERIA ITERATE(25)
                  /ROTATION varimax.
            
                        * FA tutkys1 ilman vastetta. jätetään matalahkon kommunaliteetin muuttujat pois.
                        FACTOR
                          /VARIABLES K45 
                          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
                          K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
                          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
                          /MISSING LISTWISE 
                          /ANALYSIS 
                          K4601 K4603 K4604 K4605 K47 K4801 K4802 K4803
                          /PRINT INITIAL KMO REPR EXTRACTION ROTATION
                          /PLOT EIGEN
                          /FORMAT BLANK(.00)
                          /CRITERIA FACTORS(2) ITERATE(25)
                          /EXTRACTION PAF
                          /CRITERIA ITERATE(25)
                          /ROTATION varimax.

* FA tutkys1 ilman vastetta, testataan piruuttaan promax.
FACTOR
  /VARIABLES K45 
  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
  /MISSING LISTWISE 
  /ANALYSIS 
  K1303 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 ystalue sukualue
  K5103 K5106 
  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
  /PLOT EIGEN
  /FORMAT BLANK(.00)
  /CRITERIA FACTORS(3) ITERATE(25)
  /EXTRACTION PAF
  /CRITERIA ITERATE(25)
  /ROTATION promax.
        FACTOR
          /VARIABLES K45 
          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
          K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
          /MISSING LISTWISE 
          /ANALYSIS 
          K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 ystalue sukualue
           K5106 
          /PRINT INITIAL KMO REPR EXTRACTION ROTATION
          /PLOT EIGEN
          /FORMAT BLANK(.00)
          /CRITERIA FACTORS(2) ITERATE(25)
          /EXTRACTION PAF
          /CRITERIA ITERATE(25)
          /ROTATION promax.
                        FACTOR
                          /VARIABLES K45 
                          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki
                          K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
                          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
                          /MISSING LISTWISE 
                          /ANALYSIS 
                          K4601 K4603 K4604 K4605 K47 K4801 K4802 K4803
                          /PRINT INITIAL KMO REPR EXTRACTION ROTATION
                          /PLOT EIGEN
                          /FORMAT BLANK(.00)
                          /CRITERIA FACTORS(2) ITERATE(25)
                          /EXTRACTION PAF
                          /CRITERIA ITERATE(25)
                          /ROTATION promax.

* FA  tutkys2. sis tutkys1 pääselittäjän. yllättäen epäjärjestyksen havaitseminen ja muutoksen havaitseminen eri faktorit.
  FACTOR
  /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki, 
  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
  /MISSING LISTWISE 
  /ANALYSIS K45 K1501 k1502 K1503 K1504 
  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue
  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110
  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006
  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
  /PLOT EIGEN
  /FORMAT BLANK(.00)
  /CRITERIA FACTORS(5) ITERATE(25)
  /EXTRACTION PAF
  /CRITERIA ITERATE(25)
  /ROTATION varimax.
        
        * FA  tutkys2. sis tutkys1 pääselittäjän. vuorovaikutus ja yhteishenki sulautuvat samaan faktoriin, viidenneksi tulee suvaitsevaisuus, joka ei juurikaan yhteydessä muihin. kokeillaan neljällä.
          FACTOR
          /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki, 
          K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
          /MISSING LISTWISE 
          /ANALYSIS K45 K1501 k1502 K1503 K1504 
          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue
          K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110
          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006
          /PRINT INITIAL KMO REPR EXTRACTION ROTATION
          /PLOT EIGEN
          /FORMAT BLANK(.00)
          /CRITERIA FACTORS(4) ITERATE(25)
          /EXTRACTION PAF
          /CRITERIA ITERATE(25)
          /ROTATION varimax.
        
        * FA  tutkys2. sis tutkys1 pääselittäjän. jätetään kerralla huonommat lataukset ja kommunaliteetit pois. suvaitsevaisuusfaktorilla heikot lataukset ja kommunaliteetti, ei liity muihin.
          FACTOR
          /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
          K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki, 
          K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
          /MISSING LISTWISE 
          /ANALYSIS K45 K1501 k1503 K1504 
          K1304 K1306 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803
          K5106
          K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K5002 K5003 K5004 K5005 K5006
          /PRINT INITIAL KMO REPR EXTRACTION ROTATION
          /PLOT EIGEN
          /FORMAT BLANK(.00)
          /CRITERIA FACTORS(4) ITERATE(25)
          /EXTRACTION PAF
          /CRITERIA ITERATE(25)
          /ROTATION varimax.
                
                * FA  tutkys2. sis tutkys1 pääselittäjän. kokeillaan viidettä faktoria. ja palautetaan muuttohalu koska oli tutkys1 vasteessa mahdollisena.
                  FACTOR
                  /VARIABLES K45 K1501 k1502 K1503 K1504 /* pääselittäjä naapurisuhde ja selitettävä kiintymys
                  K1303 K1304 K1306 K1307 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 K4804 K4805 K49 muutto ystalue sukualue /* tyytyväisyys, vuorovaikutus, yhteishenki, 
                  K5101 K5102 K5103 K5104 K5105 K5106 K5107 K5108 K5109 K5110  /* samankaltaisuus
                  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K37 K38  K5001 K5002 K5003 K5004 K5005 K5006 /* epäjärjestys, muutos epäjärjestyksessä, turvallisuus, sosiaalinen kontrolli
                  /MISSING LISTWISE 
                  /ANALYSIS K45 K1501 k1503 K1504 
                  K1304 K1306 K4601 K4602 K4603 K4604 K4605 K47 K4801 K4802 K4803 muutto
                  K5106
                  K3501 K3502 K3503 K3504 K3505 K3506 K3507 K3508 K3601 K3602 K3603 K3604 K3605 K3606 K3607 K3608 K5002 K5003 K5004 K5005 K5006
                  /PRINT INITIAL KMO REPR EXTRACTION ROTATION
                  /PLOT EIGEN
                  /FORMAT BLANK(.00)
                  /CRITERIA FACTORS(5) ITERATE(25)
                  /EXTRACTION PAF
                  /CRITERIA ITERATE(25)
                  /ROTATION varimax.
                * Tämä on helpommin tulkittava kuin neljän faktorin malli, koska kiintymys irtaantuu epäjärjestyksestä. faktorien välillä on kuitenkin ristiinlatauksia, vaikka muuttujien paikat ovatkin selkeitä, lähimpänä riskiä tyyt turvallisuuteen.